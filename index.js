const http = require('http');
const fs = require('fs');
const cards = require('./cards');
const PORT = 8081;

function accept(req, res) {
    let responseObj = {};

    if (req.url === '/api') {
        if (req.method === 'GET') {
            res.end(JSON.stringify(cards));
        }

        if (req.method === 'POST') {
            req.on('data', data => {
                let isWrite = true;
                let newCard = JSON.parse(data)[0];

                let isExist = cards.some(card => {
                    return card.id === newCard.id;
                })

                if (isExist) {
                    responseObj = {
                        status: '1',
                        text: 'Card exists',
                    }
                    res.end(JSON.stringify(responseObj));
                } else {
                    cards.push(newCard);
                    fs.writeFileSync('./cards.json', JSON.stringify(cards), error =>{
                        console.log(error);
                        responseObj = {
                            status: '1',
                            text: 'Write error'
                        }
                        isWrite = false;
                        res.end(JSON.stringify(responseObj));
                    })

                    if (isWrite) {
                        responseObj = {
                            status: '0',
                            text: 'Card added'
                        }
                        res.end(JSON.stringify(responseObj));
                    }
                }
            });
        }

        if (req.method === 'DELETE') {
            req.on('data', data => {
                let isWrite = true;
                let deleteCardID = JSON.parse(data)[0];

                let isExist = cards.some(card => {
                    return card.id === deleteCardID;
                })

                if (!isExist) {
                    responseObj = {
                        status: '1',
                        text: 'Card does not exist'
                    }
                    res.end(JSON.stringify(responseObj));
                } else {
                    let deletedCardIndex;
                    cards.forEach((card, index) => {
                        if (card.id === deleteCardID) {
                            deletedCardIndex = index;
                        }
                    })

                    cards.splice(deletedCardIndex, 1);

                    fs.writeFileSync('./cards.json', JSON.stringify(cards), error => {
                        console.log(error);
                        responseObj = {
                            status: '1',
                            text: 'Write error'
                        }
                        isWrite = false;
                        res.end(JSON.stringify(responseObj));
                    })

                    if (isWrite) {
                        responseObj = {
                            status: '0',
                            text: 'Card was removed'
                        }
                        res.end(JSON.stringify(responseObj));
                    }
                }
            })
        }

        if (req.method === 'PUT') {
            req.on('data', data => {
                let isWrite = true;
                let editedCard = JSON.parse(data)[0];

                let isExist = cards.some(card => {
                    return card.id === editedCard.id;
                })

                if (!isExist) {
                    responseObj = {
                        status: '1',
                        text: 'Card doesn\'t exists',
                    }
                    res.end(JSON.stringify(responseObj));
                } else {
                    let editedCardIndex;
                    cards.forEach((card, index) => {
                        if (card.id === editedCard.id) {
                            editedCardIndex = index;
                        }
                    })

                    cards.splice(editedCardIndex, 1, editedCard);

                    fs.writeFileSync('./cards.json', JSON.stringify(cards), error => {
                        console.log(error);
                        responseObj = {
                            status: '1',
                            text: 'Write error'
                        }
                        isWrite = false;
                        res.end(JSON.stringify(responseObj));
                    })

                    if (isWrite) {
                        responseObj = {
                            status: '0',
                            text: 'Card updated'
                        }
                        res.end(JSON.stringify(responseObj));
                    }
                }
            });

        }
    } else {
        res.end('Page not found');
    }

    function writeFile(res, data) {
        fs.writeFileSync('./cards.json', JSON.stringify(data), error =>{
            console.log(error);
            responseObj = {
                status: '1',
                text: 'Write error'
            }
            isWrite = false;
            res.end(JSON.stringify(responseObj));
    }

}

http.createServer(accept).listen(PORT);
console.log(`Server is running on the port: ${PORT}`);