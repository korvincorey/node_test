const fs = require('fs');
const messagesObj = require('./messagesObj.json');

let requestsMethods = {
        writeFile: function (res, data) {
        let isWrite = true;

        fs.writeFileSync('./cards.json', JSON.stringify(data), error => {
            console.log(error);
            requestsMethods.sendResponse(res, 1, messagesObj.writeError);
            isWrite = false;

        });

        return isWrite;
    },

        checkCardExist: function (card, cardArr) {
        return cardArr.some(cardItem => {
            return cardItem.id === card.id;
        })

    },

        sendResponse: function (res, status, message) {
        let responseObj = {};

        responseObj = {
            status: status,
            text: message
        }
        res.end(JSON.stringify(responseObj));
    },

        checkDataLength: function (data, req) {
        // Too much POST data, kill the connection!
        // 1e6 === 1 * Math.pow(10,6) === 1 * 1000000 ~~~ 1MB
        if (data.length > 1e6) {
            req.connection.destroy();
        }
    }
}

module.exports = requestsMethods;